@props(['title' => null])
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <script src="{{ mix('js/app.js') }}" defer></script>
    <title>{{ $title ? $title . ' | ' . config('app.title') : config('app.title') }}</title>
</head>
<body>
    <x-app.header/>
    <main>
        {{ $slot }}
    </main>
    <x-app.footer/>
</body>
</html>
